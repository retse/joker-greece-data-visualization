using CSV, DataFrames, Plots

# Read file 
file_name = "tzoker_greece_data.csv"
df = CSV.read(file_name, DataFrame)

number_cols = ["N1", "N2", "N3", "N4", "N5"]
normal_numbers = Vector{Int}(undef, nrow(df)*5)

# Get required columns for histogram
i = 1
for col in number_cols
    for row=1:nrow(df)
        normal_numbers[i] = df[row, col]
        global i += 1
    end
end
joker_col = "joker"
joker_numbers = Vector{Int}(undef, nrow(df))

for row=1:nrow(df)
    joker_numbers[row] = df[row, joker_col]
end

# Plot data
gr()
plot1 = histogram(normal_numbers, nbins=45, title="Histogram of normal numbers (1-45)\n for years 1997-2021", xlabel="Normal numbers (1-45)", ylabel="Frequency, N", color = "deepskyblue1")
plot2 = histogram(joker_numbers, nbins=20, title="Histogram of joker numbers (1-20)\n for years 1997-2021", xlabel="Joker numbers (1-20)", ylabel="Frequency, N", color = "chocolate1")
plot(plot1, plot2, layout = (2,1), legend=false)