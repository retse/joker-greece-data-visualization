import pandas
import matplotlib.pyplot as plt
import numpy as np

# Read file
file = "tzoker_greece_data.csv"
raw_data = pandas.read_csv(file, header=0)

normal_list = ['N1', 'N2', 'N3', 'N4', 'N5']
normal_numbers = []
joker_numbers = []

# Get required columns
for rows in range(0, raw_data.shape[0]):
    for cols in range(0, len(normal_list)):
        normal_numbers.append(raw_data.loc[rows, normal_list[cols]])
    joker_numbers.append(raw_data.loc[rows, 'joker'])

# Prepare and plot histogram
normal_labels, normal_counts = np.unique(normal_numbers, return_counts=True)
joker_labels, joker_counts = np.unique(joker_numbers, return_counts=True)
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.bar(normal_labels, normal_counts, align='center')
ax1.set_title('Histogram of normal numbers (1-45) for years 1997-2021')
ax1.set(ylabel='Frequency, N', xlabel='Normal numbers (1-45)')
ax1.grid()
ax2.bar(joker_labels, joker_counts, align='center', color='orange')
ax2.set_title('Histogram of joker numbers (1-20) for years 1997-2021')
ax2.set(xlabel='Joker numbers (1-20)', ylabel='Frequency, N')
ax2.grid()
plt.show()
